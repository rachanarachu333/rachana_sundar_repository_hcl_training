package com.greatlearning.miniproject;


import java.io.IOException;
import java.util.Scanner;

public class SurabiRestaurantsMain {

	public static void main(String[] args) throws IOException, InterruptedException {
		
		System.out.println("Welcome to Surabi Restaurants");
		System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
		System.out.println("Please enter your credentials");
		
		Scanner sc = new Scanner(System.in); 
		
		Login user = new Login();
		
		
		try {
			
		//to get email id	
		System.out.println("Email id =");
		user.setEmail(sc.nextLine());
		
		//to get password
		System.out.println("Password = ");
		user.setPassword(sc.nextLine());
		
		//converting first letter if the name of upper case
		String person = Character.toUpperCase(user.getPassword().charAt(0)) + user.getPassword().substring(1);
		
		
		System.out.println("Please enter A if you are admin and U if your user");
		String choice = sc.next() ;
		
		// to login as Admin
		if(choice.equalsIgnoreCase("A")) {
				if (user.getPassword().contains("admin")){
					Admin ad = new Admin();
					Thread t1 = new Thread(ad);
					t1.setName(person);
					t1.start();
					t1.join();	
					}
				else 
					System.out.println("Please log in with admin credentials");
				
			}
		//to log in as user
		else if (choice.equalsIgnoreCase("U")) {
				User customer = new User(person);
				Thread t2 = new Thread(customer);
				t2.setName(person);
				t2.start();
				t2.join();
				
					
				}
		else {
			throw new CustomException("Invaild choice");
		}
	
		
		}catch(CustomException e) {
			System.err.println(e);
			
		}
		
		System.out.println("------------------------------------------------------------------");
		

	}

}
