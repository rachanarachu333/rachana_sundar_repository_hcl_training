package com.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.bean.AddtoFavourites;



public interface FavouriteDao  extends CrudRepository<AddtoFavourites, Integer>{
	List<AddtoFavourites> findAll();
}
