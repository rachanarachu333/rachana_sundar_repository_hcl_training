package com.greatlearning.miniproject;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Date;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Formatter;
import java.util.Scanner;
import java.util.stream.Stream;

public class Admin implements Runnable  {
	
	
	Admin() {
		System.out.println("Welcome Admin");}
	
	//to display information to the Admin
	public void display() throws IOException  {
		BufferedReader reader = new BufferedReader(new FileReader("bills.txt"));
		boolean flag = true; 
					
			while(flag) {
			System.out.println("Press 1 to see all the bill for today");
			System.out.println("Press 2 to see all the bill for this month");
			System.out.println("Press 3 to see all the bill");
			System.out.println("Press 0 to log out");
		
				Scanner sc = new Scanner(System.in);
					int choice = sc.nextInt();
					switch(choice){
					case 0: flag = false;
						break;
						case 1:
								System.out.println("All the bill for today");
								
								//to get today's date in E MMM DD format 
								Date date = new Date(0);
								ZonedDateTime time1 = ZonedDateTime.now();
								 DateTimeFormatter f = DateTimeFormatter.ofPattern("E MMM dd");
								 String currentTime = time1.format(f);
								String today = "Date:-"+currentTime;
								
								String s;
								int startingLine = 0, line=0;
								
								//to get the starting line of today's date in the file 
								while((s=reader.readLine())!=null) {
									if(s.contains(today)){
									startingLine=line;
									break;
								}
								line++;
								}
								
								String ss;
								int  line1=0;
								
								//to display the contents in the file from starting today's date
								while ((ss=reader.readLine())!=null) {
												
									if(startingLine<=line1) {
									System.out.println(ss);
									}
									line1++;
								}
								break;
								
						case 2:	
							System.out.println("All the bill for this month");
							
							//to get month in MMM format 
							Calendar cal = Calendar.getInstance();
						     String month=""+ new Formatter().format("%tb", cal);
						     
						     String s2;
								int startingLine2 = 0, Line=0;
								//to get the starting line of month in the file
								while((s2=reader.readLine())!=null) {
								
								if(s2.contains(month)){
									startingLine=Line;
									break;}
								Line++;
								}
								
									int  line2=0;
									
								//to display the contents in the file from starting this month
								while ((s2=reader.readLine())!=null) {
									if(startingLine2<=Line) {
									System.out.println(s2);
									}
									Line++;
								}
								break;
						     
						case 3:
							
							//to display all the bills in the file 
							System.out.println("All the bill");
							reader.lines().forEach(System.out::println);
							break;
							
					default :System.out.println("Invalid input. please return your choice ");
							display();
						
					}
			}
		
			
		}
	
	



	@Override
	public void run() {
		// TODO Auto-generated method stub
		
		Thread t = Thread.currentThread();
		String name = t.getName();
		try {
			display();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
		
}
