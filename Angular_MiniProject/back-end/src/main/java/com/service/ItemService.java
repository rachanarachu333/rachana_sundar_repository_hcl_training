package com.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bean.Items;
import com.dao.ItemDao;

@Service
public class ItemService {

	@Autowired
	private ItemDao itemDao;
	

	public Items getFindByIdValue(int id) {
		return itemDao.findById(id);
	}

	public boolean registerBookData(Items user) {
		this.itemDao.save(user);
		return true;
	}
	
	
	public Items getbookbyid(int id) {
		return itemDao.findById(id);
	}
}
