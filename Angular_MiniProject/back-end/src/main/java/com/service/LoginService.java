package com.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bean.AddtoFavourites;
import com.bean.Cart;
import com.bean.LoginUser;
import com.dao.CartDao;
import com.dao.FavouriteDao;
import com.dao.UserDao;

@Service
public class LoginService {


	@Autowired
	public FavouriteDao favouriteDao;
	
	public boolean favRegister(AddtoFavourites addfav) {
		this.favouriteDao.save(addfav);
		return true;
	}
	
	@Autowired
	public CartDao readLaterRepository;
	
	public boolean readLaterRegister(Cart cart) {
		this.readLaterRepository.save(cart);
		return true;
	}
}
