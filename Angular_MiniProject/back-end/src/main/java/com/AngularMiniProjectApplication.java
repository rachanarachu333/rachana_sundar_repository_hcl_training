package com;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.core.JdbcTemplate;

import com.bean.AdminLogin;
import com.bean.Items;
import com.bean.LoginUser;
import com.dao.AdminLogDao;
import com.dao.ItemDao;
import com.dao.UserDao;




@SpringBootApplication
@EnableEurekaClient
public class AngularMiniProjectApplication implements CommandLineRunner {

	public static void main(String[] args) {
		
		SpringApplication.run(AngularMiniProjectApplication.class, args);
		System.out.println("Running in port 8181");
	}
	@Autowired
	private UserDao userDao;
	
	@Autowired
	private ItemDao itemDao;
	
	@Autowired
	private AdminLogDao adminLogDao;
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	public void run() {
		
	}
	@Override
	public void run(String... args) throws Exception {
		String sql="set FOREIGN_KEY_CHECKS=0";
		int result=jdbcTemplate.update(sql);
	}
	
	@Bean
	public void inserData(){
		LoginUser user1=new LoginUser("Pooja","s","pooja@gmail.com","9090909090","pooja","12345");
		userDao.save(user1);
		
		LoginUser user2=new LoginUser("Raj","R","raj@gmail.com","8181818181","raj","12345");
		userDao.save(user2);
		
		LoginUser user3=new LoginUser("Ravi","N","ravi@gmail.com","9191919191","ravi","12345");
		userDao.save(user3);
		


		Items i1=new Items("Gobi","80","3","1001");
		itemDao.save(i1);
		
		Items i2=new Items("Ice Cream","100","1","1002");
		itemDao.save(i2);
		
		Items i3=new Items("Noodels","20","1","1003");
		itemDao.save(i3);
		
		Items i4=new Items("Dosa ","150","3","1004");
		itemDao.save(i4);
		
		Items i5=new Items("Panner","50","2","1005");
		itemDao.save(i5);
		
		
	}


}
