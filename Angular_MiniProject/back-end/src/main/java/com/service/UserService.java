package com.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bean.LoginUser;
import com.dao.UserDao;

@Service
public class UserService {
	@Autowired
	public UserDao userDao;

	public boolean userLogin(LoginUser user) {
		List<LoginUser> userdetails = userDao.findAll();
		System.out.println(userdetails);
		for (LoginUser users : userdetails) {
			if (users.getEmail().equals(user.getEmail()) && users.getPassword().equals(user.getPassword())) {
				return true;
			}
		}
		return false;
	}

	public boolean registerData(LoginUser user) {
		this.userDao.save(user);
		return true;
	}

	public boolean updateUser(LoginUser loginUser) {
		System.out.println(userDao.existsById(loginUser.getid()));
		if (this.userDao.existsById(loginUser.getid())) {
			this.userDao.save(loginUser);
			return true;
		}
		return false;
	}

	public boolean addUser(LoginUser user) {
		this.userDao.save(user);
		return true;
	}

	public boolean deleteUser(int id) {
		this.userDao.deleteById(id);
		System.out.println("deleted...........");
		return true;
	}
}
