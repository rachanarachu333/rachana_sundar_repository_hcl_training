package com.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.bean.Cart;

public interface CartDao extends CrudRepository<Cart, Integer>{
	List<Cart> findAll();
}
