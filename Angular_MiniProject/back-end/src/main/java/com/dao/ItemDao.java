package com.dao;

import java.util.List;
import org.springframework.data.repository.CrudRepository;

import com.bean.Items;

public interface ItemDao extends CrudRepository<Items, Integer>{

	List<Items> findAll();

	Items findById(int id);
}
