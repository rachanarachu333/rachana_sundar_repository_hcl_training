package com.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bean.AddtoFavourites;
import com.bean.AdminLogin;
import com.bean.Cart;
import com.bean.Items;
import com.bean.LoginUser;
import com.dao.AdminLogDao;
import com.dao.CartDao;
import com.dao.FavouriteDao;
import com.dao.ItemDao;
import com.dao.UserDao;

@Service
public class AdminService {
 
	@Autowired
	private FavouriteDao favouriteDao;
	
	@Autowired
	private AdminLogDao adminLogDao;
	

	@Autowired
	private ItemDao itemDao;
	
	@Autowired
	private CartDao cartDao;

	
	public boolean loginUser(AdminLogin user) {
		List<AdminLogin> userdetails=adminLogDao.findAll();
		System.out.println(user);
		for(AdminLogin users:userdetails) {
			if(users.getEmail().equals(user.getEmail())&& users.getPassword().equals(user.getPassword())) {
				return true;
			}
		}
		return false;	
	}
	public boolean registerData(AdminLogin user) {
		this.adminLogDao.save(user);
		return true;
	}
	public boolean addFav(AddtoFavourites addfav) {
		this.favouriteDao.save(addfav);
		return true;
	}
	public boolean addCart(Cart cart) {
		this.cartDao.save(cart);
		return true;
	}
	public boolean addItem(Items item) {
		this.itemDao.save(item);
		return true;
	}

	public boolean deleteItem(int id) {
		this.itemDao.deleteById(id);
		System.out.println("deleted...........");
		return true;
}
	public boolean updateItem(Items items) {
		System.out.println(itemDao.existsById(items.getId()));
		if (this.itemDao.existsById(items.getId())) {
			this.itemDao.save(items);
			return true;
		}
		return false;
	}

	public boolean deleteFav(int id) {
		if (this.favouriteDao.existsById(id)) {
			this.favouriteDao.deleteById(id);
			return true;
		}
		return false;
	}
	public boolean deleteLater(int id) {
		if (this.cartDao.existsById(id)) {
			this.cartDao.deleteById(id);
			return true;
		}
		return false;
	}
	public List<AddtoFavourites> addedFavourite(String email){
		List<AddtoFavourites> addedfav=favouriteDao.findAll();
		List<AddtoFavourites> added=new ArrayList<>();
		for(AddtoFavourites list:addedfav) {
			if(list.getUser_email().equals(email)) {
				added.add(list);
			}
		}
		return added;
	}
	public List<Cart> addedCart(String email){
		List<Cart> addedfav=cartDao.findAll();
		List<Cart> added=new ArrayList<>();
		for(Cart list:addedfav) {
			if(list.getUser_email().equals(email)) {
				added.add(list);
			}
		}
		return added;
	}
	
}
