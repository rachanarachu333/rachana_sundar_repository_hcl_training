package com.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.bean.LoginUser;

public interface UserDao extends CrudRepository<LoginUser, Integer>{
	List<LoginUser> findAll();
	
	LoginUser findByEmail(String email);
	
	LoginUser findById(int id);
}
