package com.greatlearning.miniproject;

public class Menu {
	
 private int itemCode;
 private String food ;
 private int quantity ;
 private double price;

 
 public Menu() {
	super();
	// TODO Auto-generated constructor stub
}
 
public Menu(int itemCode, String food, int quantity, double price) {
	super();
	this.itemCode = itemCode;
	this.food = food;
	this.quantity = quantity;
	this.price = price;
}

public int getItemCode() {
	return itemCode;
}
public void setItemCode(int itemCode) {
	this.itemCode = itemCode;
}
public String getFood() {
	return food;
}
public void setFood(String food) {
	this.food = food;
}
public int getQuantity() {
	return quantity;
}
public void setQuantity(int quantity) {
	this.quantity = quantity;
}
public double getPrice() {
	return price;
}
public void setPrice(double price) {
	this.price = price;
}
@Override
public String toString() {
	return "Menu [itemCode=" + itemCode + ", food=" + food + ", quantity=" + quantity + ", price=" + price + "]";
} 
 
 
	
}
